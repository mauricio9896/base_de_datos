import firebase_admin
from firebase_admin import credentials
from firebase_admin import db


from pyfirmata import Arduino, util
from tkinter import *
from PIL import Image
from PIL import ImageTk
import time

canvas_width =  425
canvas_height = 475

sizex_circ=60
sizey_circ=60

cont=0
prom=0

placa = Arduino ('COM3')
it = util.Iterator(placa)
it.start()


sensor1= placa.get_pin('a:0:i')
sensor2= placa.get_pin('a:1:i')
sensor3= placa.get_pin('a:2:i')



led1 = placa.get_pin('d:9:o')
led2 = placa.get_pin('d:10:o')
led3 = placa.get_pin('d:11:o')


time.sleep(0.5)
ventana = Tk()
ventana.geometry('450x500')
ventana.title("UI para sistemas de control")

# Fetch the service account key JSON file contents
cred = credentials.Certificate('secreto/llave.json')
# Initialize the app with a service account, granting admin privileges
firebase_admin.initialize_app(cred, {
    'databaseURL': 'https://base-de-daos.firebaseio.com/'
})

draw = Canvas(ventana, width=canvas_width, height=canvas_height,bg="plum1")
draw.place(x = 10,y = 10)

b=Label(ventana,text=":")

img = Image.open("C:/Users/LENOVO/Desktop/herramientas/usa.png")
img = img.resize((200,135))
photoImg=  ImageTk.PhotoImage(img)
b.configure(image=photoImg)
b.place(x = 120,y = 345)

def update_label():
    global cont
    global sensor1
    
    cont=sensor1.read()                
    cont_indicador1['text']=str(cont)
    
    global cont1
    global sensor2
    
    cont1=sensor2.read()                 
    cont_indicador2['text']=str(cont1)


    global cont2
    global sensor3
    
    cont2=sensor3.read()
    cont_indicador3['text']=str(cont2)
    
#############################################################################

    if(cont>0.5) or (cont1>0.5) or (cont2>0.5):
        alerta.configure(text="ALERTA !!!!!!",fg="red",font=("Rockwell Extra Bold",18),bg="white")

    if(cont<=0.5) and (cont1<=0.5) and (cont2<=0.5):
        alerta.configure(text=" ",bg="plum1")
        
#########################################################################        

    if(cont>0.5):
        
        led1.write(1)
        draw.itemconfig(led1_draw, fill="red")

        estado1='ON'

        cont_indicador4.configure(text="ON",fg="black")

        ref = db.reference("sensores")
        ref.update(
            {
                'ADC1':
                {
                    'VALOR':cont,
                    'LED':estado1
                }
            }
                ) 
        
    if(cont<=0.5):
        led1.write(0)
        draw.itemconfig(led1_draw, fill="white")

        estado1='OFF'

        cont_indicador4.configure(text="OFF",fg="black")

        ref = db.reference("sensores")
        ref.update(
            {
                'ADC1':
                {
                    'VALOR':cont,
                    'LED':estado1
                }
            }
                ) 
        
    if(cont1>0.5):

        led2.write(1)
        draw.itemconfig(led2_draw, fill="red")

        estado2='ON'

        cont_indicador5.configure(text="ON",fg="black")

        ref = db.reference("sensores")
        ref.update(
            {
                'ADC2':
                {
                    'VALOR':cont1,
                    'LED':estado2
                }
            }
                )
        
    if(cont1<=0.5):
        led2.write(0)
        draw.itemconfig(led2_draw, fill="white")
        estado2='OFF'
        cont_indicador5.configure(text="OFF",fg="black")

        ref = db.reference("sensores")
        ref.update(
            {
                'ADC2':
                {
                    'VALOR':cont1,
                    'LED':estado2
                }
            }
                )
        
    if(cont2>0.5):
        led3.write(1)
        draw.itemconfig(led3_draw, fill="red")
        estado3='ON'
        cont_indicador6.configure(text="ON",fg="black")

        ref = db.reference("sensores")
        ref.update(
            {
                'ADC3':
                {
                    'VALOR':cont2,
                    'LED':estado3
                }
            }
                )
    if(cont2<=0.5):
        led3.write(0)
        draw.itemconfig(led3_draw, fill="white")
        estado3='OFF'
        cont_indicador6.configure(text="OFF",fg="black")

        ref = db.reference("sensores")
        ref.update(
            {
                'ADC3':
                {
                    'VALOR':cont2,
                    'LED':estado3
                }
            }
                )
        
    ventana.update()
    ventana.after(5000,update_label)

##############################################################################################

titulo=Label(ventana,text="SISTEMA DE CONTROL ",font=("Rockwell Extra Bold",18),fg="black")
titulo.place(x=60,y=15)
titulo.configure(bg='pale turquoise')

titulo1=Label(ventana,text="MAURCIO BUITRAGO",font=("Rockwell Extra Bold",14),fg="black")
titulo1.place(x=100,y=40)
titulo1.configure(bg='pale turquoise')
###################################################################################################

led1_draw=draw.create_oval(10,185,10+sizex_circ,185+sizey_circ,fill="white")
led2_draw=draw.create_oval(190,185,190+sizex_circ,185+sizey_circ,fill="white")
led3_draw=draw.create_oval(360,185,360+sizex_circ,185+sizey_circ,fill="white")

################################################################################
cont_indicador1= Label(ventana, text='0',bg='cadet blue1', font=("Arial Bold", 15), fg="white", width=5)
cont_indicador1.place(x=20, y=120)

start_button1=Label(ventana,text="ADC1",bg="plum1")
start_button1.place(x=35, y=160)

cont_indicador2= Label(ventana, text='0',bg='cadet blue1', font=("Arial Bold", 15), fg="white", width=5)
cont_indicador2.place(x=200, y=120)

start_button2=Label(ventana,text="ADC2",bg="plum1")
start_button2.place(x=215, y=160)

cont_indicador3= Label(ventana, text='0',bg='cadet blue1', font=("Arial Bold", 15), fg="white", width=5)
cont_indicador3.place(x=370, y=120)

start_button3=Label(ventana,text="ADC3",bg="plum1")
start_button3.place(x=385, y=160)
###############################################################################################
alerta=Label(ventana,text=" ",bg="plum1")
alerta.place(x=130,y=297)

cont_indicador4= Label(ventana, text=' ',bg='plum1', font=("Arial Bold", 15), fg="white", width=5)
cont_indicador4.place(x=20, y=260)

cont_indicador5= Label(ventana, text=' ',bg='plum1', font=("Arial Bold", 15), fg="white", width=5)
cont_indicador5.place(x=200, y=260)

cont_indicador6= Label(ventana, text=' ',bg='plum1', font=("Arial Bold", 15), fg="white", width=5)
cont_indicador6.place(x=370, y=260)


update_label()


ventana.mainloop()
